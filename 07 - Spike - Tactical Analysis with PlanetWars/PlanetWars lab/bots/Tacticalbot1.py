from dis import dis
from math import dist
from random import choice
from tkinter.messagebox import NO
from turtle import distance
from unittest import skip

class Tacticalbot1(object):
    
    def update(self, gameinfo):
        
        if gameinfo.my_fleets:
            return 
        
        if gameinfo.my_planets and gameinfo.not_my_planets:
        # select random target and destination

        # creating not my plannets loop to find the minimum
            

            best_distance = None
            src_obj = None
            dest_obj = None


            for my_planet in list(gameinfo.my_planets.values()):
                for my_enemy in list(gameinfo.not_my_planets.values()):
                    planet_distance = my_planet.distance_to(my_enemy)
                    
                    if not best_distance or best_distance < planet_distance:
                        best_distance = planet_distance
                    else:
                        src_obj = my_planet
                        dest_obj = my_enemy

                if best_distance:
                    if not src_obj or dest_obj:
                        dest_obj = choice(list(gameinfo.not_my_planets.values()))
            
                        src_obj = choice(list(gameinfo.my_planets.values()))
                        
                gameinfo.planet_order(src_obj, dest_obj, int(src_obj.num_ships * 0.75) )

            
        pass