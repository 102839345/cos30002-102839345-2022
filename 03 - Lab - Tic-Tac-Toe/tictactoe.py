from random import randrange
import time
from unittest import skip  #use of time pause to give the user more time to see what is going on

WINNING_COMBOS = (  #this array has all the winning combination
    (0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6),
    (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)
)


board_dimension = [' '] * 9     #the board

players = {     # the players and the their names
    'x': 'ALEXA',
    'o': 'SIRI',
}
current_player = ''

winner = None
move = None

HR = '-' * 40


def move_check():   #this function will check for valid moves and retures True
                    # or prints an error message
    global move
    try:
        move = int(move)
        if board_dimension[move] == ' ':
            return True
        else:
            return False
    except:
        print('>> STOP AND CHECK THE AI BEFORE IF DESTROYS THE WORLD...!')
        return False

def result_check():  # checks if there are any winners 
    for row in WINNING_COMBOS:
        if board_dimension[row[0]] == board_dimension[row[1]] == board_dimension[row[2]] != ' ':
            return board_dimension[row[0]]

    if ' ' not in board_dimension:
        return 'tie'

    return None

def ai_move():   # this is a normal random number generator 
    return randrange(9)

def ai_move2():    # this is a Bot that looks for possible winning moves and denys the opponent winning chance at the same time
    
    for row in WINNING_COMBOS:
        if board_dimension[row[0]] == board_dimension[row[1]] != ' ':
            if board_dimension[row[2]] == ' ':
                return row[2]
        if board_dimension[row[1]] == board_dimension[row[2]] != ' ':
            if board_dimension[row[0]] == ' ':
                return row[0]
        if board_dimension[row[0]] == board_dimension[row[2]] != ' ':
            if board_dimension[row[1]] == ' ':
                return row[1]

    return randrange(9)


def input():        #basic input function
    global move
    if current_player == 'x':
        move = ai_move2()
    else:
        move = ai_move()

    if not move_check():
        input()

def update():       #function for updating the model
    global winner, current_player

    if move_check():
        board_dimension[move] = current_player

        winner = result_check()

        if current_player == 'x':
            current_player = 'o'
        else:
            current_player = 'x'


def render():       #this function renders the updated board on screen
    print('    %s | %s | %s' % tuple(board_dimension[:3]))
    print('   -----------')
    print('    %s | %s | %s' % tuple(board_dimension[3:6]))
    print('   -----------')
    print('    %s | %s | %s' % tuple(board_dimension[6:]))

    if winner is None:
        print('The current player is: %s' % players[current_player])


def welcome_screen():   #a welcome screen message printing function
    message = '''
...Welcome to TiC-TaK-ToE...
You are about to withness the battle between ALEXA and SIRI
Sit back and Enjoy the Game
'''
    print(message)
    print(HR)


if __name__ == '__main__':      #the main function
    welcome_screen()
    
    toss = randrange(2)

    if toss == 0:
        current_player = 'x'
    else:
        current_player = 'o'

    render()

    while winner is None:
        time.sleep(1)
        input()
        update()
        render()

    print(HR)
    if winner == 'tie':
        print('...TIE...')
    elif winner in players:
        print('%s is the WINNER!!!' % players[winner])  

    print(HR)
    print('...Game over....')  # the end
    