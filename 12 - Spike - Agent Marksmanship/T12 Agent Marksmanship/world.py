from os import remove
from statistics import mode
from vector2d import Vector2D, Point2D
from matrix33 import Matrix33
from graphics import egi


class World(object):

    def __init__(self, cx, cy):
        self.cx = cx
        self.cy = cy
        self.target = Vector2D(cx / 2, cy /2)
        self.hunter = None
        self.agents = []
        self.paused = False
        self.show_info = True
        self.player = []
        self.enemy = []
        self.mode = 'rifle'
        self.impact_range = 20
        self.delta = None

    def update(self, delta):
        if not self.paused:
            for agent in self.player:
                agent.update(delta)
                
            for enemy in self.enemy:
                enemy.update(delta)

                
    def render(self, delta):
        egi.circle(Point2D(50, self.cy /2), 50)

        for enemy in self.enemy:
            enemy.render()
            

        for agent in self.player:
            agent.render()

            if self.wrap_around(agent.pos) is True:
                self.player.remove(agent)

        '''for agent in self.player:
            for enemy in self.enemy:
                if (agent.pos - enemy.pos).length() < self.impact_range:
                    self.enemy.remove(enemy)
                    self.player.remove(agent)
                    break'''

        for agent in self.player:
            impact = False
            for enemy in self.enemy:
                if (agent.pos - enemy.pos).length() < self.impact_range:    
                    self.enemy.remove(enemy)
                    impact = True
                    #self.player.remove(agent)
            if impact:
                self.player.remove(agent)
                break

            #set blast range code

        if self.target:
            egi.red_pen()
            egi.cross(self.target, 10)
            

        if self.show_info:
            infotext = ', '.join(set(agent.mode for agent in self.player))
            egi.white_pen()
            egi.text_at_pos(0, 0, infotext)
            
   

    def wrap_around(self, pos):
        if pos.x > self.cx or pos.x < 0 or pos.y < 0 or pos.y > self.cy:
            return True

    def wrap_around_Enemy(self, pos):
        ''' Treat world as a toroidal space. Updates parameter object pos '''
        max_x, max_y = self.cx, self.cy
        if pos.x > max_x:
            pos.x = pos.x - max_x
        elif pos.x < 0:
            pos.x = max_x - pos.x
        if pos.y > max_y:
            pos.y = pos.y - max_y
        elif pos.y < 0:
            pos.y = max_y - pos.y

    def transform_points(self, points, pos, forward, side, scale):
        wld_pts = [pt.copy() for pt in points]
        mat = Matrix33()
        mat.scale_update(scale.x, scale.y)
        mat.rotate_by_vectors_update(forward, side)
        mat.translate_update(pos.x, pos.y)
        mat.transform_vector2d_list(wld_pts)
        return wld_pts
        

    def transform_point(self,point,pos,forward,side):
        wld_pt = point.copy()
        mat = Matrix33()
        mat.rotate_by_vectors_update(forward, side)
        mat.translate_update(pos.x, pos.y)
        mat.transform_vector2d(wld_pt)
        return wld_pt
