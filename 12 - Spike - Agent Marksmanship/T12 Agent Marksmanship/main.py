from graphics import egi, KEY
from pyglet import window, clock
from pyglet.gl import *

from vector2d import Vector2D
from world import World
from Game import Game, WEAPON_MODE, Enemy
import time

WEAPON_MODE_2 = {
    KEY._1: 'rifle',
    KEY._2: 'rocket',
    KEY._3: 'hand_gun',
    KEY._4: 'grenade',
}

def on_mouse_motion(x , y, dx, dy):
    world.target = Vector2D(x ,y)

def on_mouse_press(x, y, button, modifiers):
    if button == 1:  # left
        world.target = Vector2D(x, y)


def on_resize(cx, cy):
    world.cx = cx
    world.cy = cy

def Append_ammo(self):
        world.player.append(Game(world, world.mode))

def on_key_press(symbol , modifier):
    #mode = 'rifle'
    #prev_time = 0
    #prev_time = clock.tick()
    
    if symbol == KEY.P:
        world.paused = not world.paused
    elif symbol in WEAPON_MODE:
        mode = WEAPON_MODE[symbol]
        world.mode = mode

        if mode == 'rifle':
            world.impact_range = 20 
        elif mode == 'rocket':
            world.impact_range = 150
        elif mode == 'hand_gun':
            world.impact_range = 20
        elif mode == 'grenade':
            world.impact_range = 200
        else:
            world.impact_range = 20

    elif symbol == KEY.SPACE:
        
        if world.mode == 'rifle':
            time_gap =  0.1
        elif world.mode == 'rocket':
            time_gap =  2.0
        elif world.mode == 'hand_gun':
            time_gap = 1.0
        elif world.mode == 'grenade':
            time_gap = 3.0
        else:
            time_gap = 0.1

        #print(prev_time)
        #clock.schedule_interval(Append_ammo, time_gap)
        clock.schedule_once(Append_ammo, time_gap)
        #clock.schedule_interval_soft(Append_ammo, time_gap)
        #clock.set_default()
        #world.player.append(Game(world, world.mode))
            #prev_time = clock.tick()



    elif symbol == KEY.ENTER:
        world.enemy.append(Enemy(world))

    

if __name__ == '__main__':

    win = window.Window(width=1000, height=600, vsync=True, resizable=False)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    #prev_time = clock.time()

    egi.InitWithPyglet(win)

    world = World(1000, 600)

    win.push_handlers(on_mouse_motion)
    #win.push_handlers(on_mouse_press)
    win.push_handlers(on_key_press)
    #win.push_handlers(on_resize)

    fps_display = window.FPSDisplay(win)

    for x in range(50):
        world.enemy.append(Enemy(world))

    while not win.has_exit:
        win.dispatch_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        # show nice FPS bottom right (default)
        delta = clock.tick()
        world.update(delta)
        world.render(delta)
        fps_display.draw()
        # swap the double buffer
        win.flip()