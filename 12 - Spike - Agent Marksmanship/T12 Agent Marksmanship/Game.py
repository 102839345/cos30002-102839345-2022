from asyncio import tasks
from cgitb import grey
from tkinter import Scale
from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path


WEAPON_MODE = {
    KEY._1: 'rifle',
    KEY._2: 'rocket',
    KEY._3: 'hand_gun',
    KEY._4: 'grenade',
}

class Game(object):

    def __init__(self, world=None, mode='rifle',   scale=10.0, mass=5.0 ):
        self.world = world
        self.mode = mode
        
        self.pos = Vector2D(50, world.cy /2)
        self.vel = Vector2D()
        self.heading = self.world.target
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.acceleration = Vector2D()  # current steering force
        self.mass = mass
        # limits?
        self.max_speed = 1000.0
        #self.max_speed = 20.0 * scale
        self.max_force = 500.0
        # data for drawing this agent
        self.color = 'RED'
        self.path = Path()

        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]
        self.acceleration = self.calculate()

    def calculate(self):
        mode = self.mode
        if mode == 'rifle':
            force = self.rifle(self.world.target)
            
        elif mode == 'rocket':
            force = self.rocket(self.world.target)
        elif mode == 'hand_gun':
            force = self.hand_gun(self.world.target)
        elif mode == 'grenade':
            force = self.grenade(self.world.target)

        else:
            force = self.rifle()

        accel = Vector2D()
        self.acceleration = accel
        return force

   
    def render(self, color = None):
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos, self.heading, self.side,self.scale)
        egi.closed_shape(pts, True)
        #egi.circle(Point2D(50, 230), 5)

    def update(self, delta):
        ''' update vehicle position and orientation '''
        acceleration = self.acceleration
        self.delta = delta
        # new velocity
        self.vel += acceleration * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.lengthSq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        
    def speed(self):
        return self.vel.length()


    def rifle(self, target_pos):
        self.scale = Vector2D(5, 5)

        self.max_speed = 5000
        
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)


    def rocket(self, target_pos):
        self.scale = Vector2D(7, 7)

        self.max_speed = 2000

        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    def hand_gun(self, target_pos):
        self.scale = Vector2D(5, 5)

        self.max_speed = 5000
        
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    def grenade(self, target_pos):
        self.scale = Vector2D(7, 7)

        self.max_speed = 500
        
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    
class Enemy(object):
    
    
    DECELERATION_SPEEDS = {
        'slow': 0.5,
        'normal' : 1,
        'fast' : 2
        
    }


    def __init__(self, world=None,scale=20.0, mass=1.0, mode='wander'):
        
        self.world = world
        self.mode = mode

        
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(self.world.cx), randrange(self.world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)
        self.force = Vector2D()
        self.accel = Vector2D()
        self.mass = mass

        
        self.color = 'GREEN'
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]

       
        self.path = Path()
        self.randomise_path()
        self.waypoint_threshold = 0.0

        
        self.wander_target = Vector2D(1, 0)
        self.wander_dist = 1.0 * scale
        self.wander_radius = 1.0 * scale 
        self.wander_jitter = 10.0 * scale
        self.bRadius = scale
        
        self.max_speed = 2.0 * scale
        self.max_force = 2.0

        
        self.max_speed = 20.0 * scale
        
        self.show_info = False

    def randomise_path(self):
        cx = self.world.cx
        cy = self.world.cy
        margin = min(cx, cy) * (1/6)
        self.path.create_random_path(randrange(3,10), margin, margin, cx, cy)


    
    def calculate(self,delta):
        
        mode = self.mode
        if mode == 'seek':
            force = self.seek(self.world.target)
        elif mode == 'arrive_slow':
            force = self.arrive(self.world.target, 'slow')
        elif mode == 'arrive_normal':
            force = self.arrive(self.world.target, 'normal')
        elif mode == 'arrive_fast':
            force = self.arrive(self.world.target, 'fast')
        elif mode == 'flee':
            force = self.flee(self.world.target)
        elif mode == 'pursuit':
            force = self.pursuit(self.world.hunter)
        elif mode == 'wander':
            force = self.wander(delta)
            
        elif mode == 'follow_path':
            force = self.follow_path()
        elif mode == 'hide':

            force = self.hide(self.world.hunter.pos)
            
            
        else:
            force = Vector2D()
            
        self.force = force
        return force


    def follow_path(self):
        
        self.path.render()
        if self.path.is_finished():
            return self.arrive(self.path.current_pt(), 'slow')
        else:
            if (self.path.current_pt() - self.pos).length() < 50:
                self.path.inc_current_pt()
            else:
                return self.seek(self.path.current_pt())
        
        return Vector2D()    

    def update(self, delta):
        ''' update vehicle position and orientation '''
        
        force = self.calculate(delta) 
        
        force.truncate(self.max_force)
        
        self.accel = force / self.mass
        
        self.vel += self.accel * delta
        
        self.vel.truncate(self.max_speed)
        
        self.pos += self.vel * delta
        
        if self.vel.lengthSq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        
        self.world.wrap_around_Enemy(self.pos)

    def render(self, color=None):
        if self.mode == 'follow_path':
            self.path.render()
            
        if self.mode == 'wander':
            wnd_pos = Vector2D(self.wander_dist, 0)
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)       
            wnd_pos = (self.wander_target + Vector2D(self.wander_dist, 0))
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)

        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos, self.heading, self.side,self.scale)
       
        egi.closed_shape(pts)

        

        if self.show_info:
            s = 0.5

            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)

            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)

            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)

    def speed(self):
        return self.vel.length()

    #--------------------------------------------------------------------------

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)


    def wander(self, delta):
        ''' Random wandering using a projected jitter circle. '''
        
        wt = self.wander_target

        jitter_tts = self.wander_jitter * delta
        
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        
        wt.normalise()
        
        wt *= self.wander_radius
        
        target = wt + Vector2D(self.wander_dist, 0)
        
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)
       
        return self.seek(wld_target)
       

