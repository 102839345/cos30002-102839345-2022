# 02 - Lab - FSM
from enum import Flag
import random
#variables
travel = 0
threat = 0
conflict = 0
obstacle = 0
hunger = 0
shot = 0
steady= 1

states = ['standing','walking','eating','fighting','running','falling']
current_state = 'standing'

health = 100
game_time = 0
alive = True

while alive and game_time < 40:
    game_time += 1

    if current_state is 'standing':
        print('...')
        health -= 1
        hunger += 1
        conflict = random.randint(0, 1)
        if conflict == 1:
            current_state = 'fighting'

        if health < 70 or hunger > 10:
            current_state = 'eating'

    if current_state is 'fighting':
        print('**fighting**')
        health -=10
        hunger += 5
        steady = random.randint(0, 1)
        threat = random.randint(1, 5)
        if threat > 3:
            current_state = 'running'
        if health == 0:
            alive = False
        if steady == 0:
            print('!...BONK...!')
            current_state = 'falling'

    if current_state is 'walking':
        print('walking...')
        health -= 1
        hunger += 2
        if health < 70 or hunger > 10:
            current_state = 'eating'

    if current_state is 'eating':
        print('nom nom nom...')
        hunger = 0
        health = 100
        current_state = 'standing' 

    if current_state is 'running':
        print('...Running...')
        threat = random.randint(1, 5)
        if threat < 3:
            current_state = 'standing'
            conflict = 0 

    if current_state is 'falling':
        print('I am Down...')
        steady = random.randint(0, 1)
        if steady == 1:
            current_state = 'standing'
        if health < 70 or hunger > 10:
            current_state = 'eating' 

print('\n-- The End --')
print(game_time)